import React from "react";
import "./Navside.css";
import { Link } from "react-router-dom";


export default function Navside() {
  return (
    <>
      <div className="nav shadow-lg">
        <div className="d-flex flex-row p-0 m-0 h-25 justify-content-center w-100">
          <h3 className="d-flex flex-start p-2 "> Accueil </h3>
        </div>
        <div className="d-flex flex-row h-75">
          <div className="d-flex flex-column justify-content-start">
            <ul>
              <li>
                <Link to="/home" style={{ textDecoration: 'none' }}> <i class="fa fa-home" aria-hidden="true"></i>&nbsp; Home</Link>
              </li>
              <li>
                <Link to="/content" style={{ textDecoration: 'none' }}> <i class="fa fa-list" aria-hidden="true"></i>&nbsp; Liste des tâches</Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  );
}
