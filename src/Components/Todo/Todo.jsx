import React from "react";
import "./Todo.css"

export default function Todo(props) {
  return (
    <div className="d-flex flex-row justify-content-center">
      <div
        class="alert alert-success w-50 d-flex flex-row justify-content-around p-2"
        role="alert"
      >
        <span className="id">{props.todo.id}</span> &#x7C; <span><b>{props.todo.nom}</b></span>
        <span>
          <button className="btn btn-danger" onClick={()=> props.delete(props.todo.id)}> Delete</button>
        </span>
      </div>
    </div>
  );
}
