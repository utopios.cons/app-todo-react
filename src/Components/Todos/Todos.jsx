import React from "react";
import Todo from "../Todo/Todo";

export default function Todos(props) {
  return (
    <div className="d-flex flex-column justify-content-center">
      {props.todos?.map((element, index) => (
        <Todo todo={element} key={index} delete={props.supprime}/>
      ))}
    </div>
  );
}
