/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import "./Header.css";
import {Link} from "react-router-dom"
//import logo from '/../../assets/logo-todo.png'

function Header() {
  const logo = require("../../assets/logo-todo.png");

  return (
    <>
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">
          <img src={logo} alt="logo" width="50px"></img>
        </a>
        <button
          class="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"> </span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <Link class="nav-link" to="/home">
                Home
              </Link>
            </li>
            <li class="nav-item">
              <Link class="nav-link" to="/content">
                Liste des tâches
              </Link>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                About
              </a>{" "}
            </li>
          </ul>{" "}
        </div>{" "}
      </nav>
    </>
  );
}

export default Header;
