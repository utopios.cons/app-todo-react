import "./App.css";
import Header from "./Components/Header/Header";
import Navside from "./Components/NavSide/Navside";
import {Routes} from "react-router-dom";
import {Route} from "react-router-dom";
import Home from "./Containers/Home/Home";
import Content from "./Containers/Content/Content"

function App() {
  return (
    <>
      <Header className="header pl-4" />
      <div className="App">
        <Routes>
          <Route path="/home" element={<Home />} />
          <Route path="/content" element={<Content />} />
        </Routes> 
      </div>
      <Navside className="navside" />
    </>
  );
}

export default App;
