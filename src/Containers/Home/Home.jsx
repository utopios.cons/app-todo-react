import React from "react";
import "./Home.css";
//import { Fragment } from "react";
import { useNavigate } from "react-router-dom";

export default function Home() {
  const navigate = useNavigate();

  return (
    <div className="d-flex flex-row justify-content-center p-4">
      <div class="jumbotron shadow-lg w-50 p-3 ">
        <h1 class="display-4">Bienvenue !</h1>
        <p class="lead">
          Cette application va vous permettre de mieux vous organiser en créant
          des tâches. Vous aurez aussi la possibilité de supprimer des tâches si
          vous le souhaitez.
        </p>
        <hr class="my-4" />
        <p>A vous de jouer !!</p>
        <button
          onClick={() => navigate("/content")}
          className="btn btn-primary"
        >
          Go
        </button>
      </div>
    </div>
  );
}
