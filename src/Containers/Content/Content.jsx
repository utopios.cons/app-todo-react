import React, { useEffect } from "react";
import { useState } from "react";
import { get, supprimeTodo } from "../../Services/ApiService";
import { post } from "../../Services/ApiService";
import Todos from "../../Components/Todos/Todos";
import { v4 as uuidv4 } from "uuid";

export default function Content() {
  const [todos, setTodos] = useState();
  const [nom, setNom] = useState();
  const [id, setId] = useState(uuidv4());

  useEffect(() => {
    get("todo").then((res) => {
      setTodos(res.data);
    });
  });

  const submit = () => {
    const id2 = uuidv4();
    setId(id2);
    const todo = {
      id: id,
      nom: nom,
    };
    console.log(todo);

    post("creation", todo).then((res) => {
      const response = res.data;
      if (response != null) {
        clear();
      }
      console.log(response);
    });
  };

  const clear = () => {
    setNom(" ");
  };

  const supprime = (id) => {
    supprimeTodo("delete-todo/", id).then((res) => {
      const response = res.data;
      console.log(response);
    });
  };

  return (
    <>
      <h1 className="text-center pt-4"> Liste des tâches </h1>
      <div>
        <div className="d-flex flex-row justify-content-center mt-4">
          <div class="input-group mb-3 w-50 ">
            <input
              type="text"
              class="form-control"
              placeholder="Saisissez vos tâches"
              onChange={(e) => setNom(e.target.value)}
            />
            <div class="input-group-append">
              <button
                class="btn btn-outline-secondary"
                type="button"
                onClick={submit}
              >
                Button
              </button>
            </div>
          </div>
        </div>
      </div>
      <Todos todos={todos} supprime={supprime} />
    </>
  );
}
